# MATH4105 Geodesics

## What is a geodesic?
	- The shortest path between two points
		- example: a line in Euclidean space

- Can be calculated in arbitrary manifolds using calculus of variations

If distance squared in any manifold was given by

\[
\begin{aligned}
ds^2 &= g_{\mu \nu} dx^\mu dx^\nu ,
\end{aligned}
\]

it would be the case that \(ds^2\) could be positive, negative or \( 0 \) in a pseudo-Riemannian manifold. This is undesirable, as \(ds^2\) is supposed to represent a length, which physically, conceptually, should be positive semi-definite (greater than or equal to zero).
	- [Q]: Would there be any point to having a signed length? What would this communicate if it was conventional/used?

To fix this, \( ds^2 \) is written as
\[
\begin{aligned}
ds^2 &= \epsilon g_{\mu \nu} dx^\mu dx^\nu, \tag{1}
\end{aligned}
\]


where
\[
\begin{aligned}
\epsilon &= \begin{cases}
+1, g_{\mu \nu} dx^\mu dx^nu > 0 \\
-1, g_{\mu \nu} dx^\mu dx^nu < 0 .
\end{cases}
\end{aligned}
\]

If \( g_{\mu \nu} dx^\mu dx^nu = 0\), the value of \( \epsilon \) is irrelevant. This is current convention used in this lecture, but may not be correct.

It is thus the case that
\[
\begin{aligned}
ds &= \sqrt{\epsilon g_{\mu \nu} dx^\mu dx^\nu}
\end{aligned}
\]
is the distance between any two points.

Now suppose there is a curve $x^\mu (t)$ that is parametrized by the variable $t$. This curve can be more compactly labelled $C \equiv x^\mu (t)$.

As $t$ increases, it indexes elements of the set of points that form curve $C$ in a 'natural' order.

Parameter $t$ can take values between $t_1 \leq t \leq t_2$, with $t_1$ indexing the first element of $C$, $C(t_1)$ being the first element of $C$, and $t_2$ indexing the last via $C(t_2)$.

The shorthand definition has been made that
\[
\begin{aligned}
\frac{dx^\mu (t)}{dt} dt &\equiv dx^\mu(t).
\end{aligned}
\]

A further shorthand definition can be made that
\[
\begin{aligned}
dx^\mu(t) &\equiv p^\mu(t) dt
\end{aligned}
\]

where $p^\mu$ is thus a tangent vector to the curve at the point $t$.

Equation $(1)$ can thus be rewritten as
\[
\begin{aligned}
ds &= \sqrt{\epsilon g_{\mu \nu} p^\mu \,dt\, p^\nu dt} \\
&= \sqrt{\epsilon g_{\mu \nu} p^\mu p^\nu} dt.
\end{aligned}
\]

This assumes the value of $\epsilon$ is constant along the curve $x^\mu(t)$, which needn't be true for [what types of manifolds?].
- [Q] What manifolds, or in what situations, may $\epsilon$ not be constant?

The length of the curve $C$ between two points labelled $\alpha$ and $\beta$ can be calculated via
\[
\begin{aligned}
S_{\alpha \beta} &= \int_\alpha^\beta ds &= \int_{t_1}^{t_2} \sqrt{\epsilon g_{\mu \nu} p^\mu p^\nu} dt .
\end{aligned}
\]

When the derivative of the length of the

When the curve is such that the variation in its length caused by changing its path an amount approaching $0$ approaches $0$, the curve is the shortest possible curve in that manifold.

- [Q] Why is this? Is this because all cancel out, because the changes in length are signed somehow, or because this approach $0$ happens in the way I just realized this afternoon and later read in QCD Feynman's? What is meant by lecturer?
- [A]
[image 1]


'The change of the length as the curve changes a very small amount is $0$.'
\[
\begin{aligned}
\frac{ \delta}{\delta C} \int_\alpha^\beta ds &= 0
\end{aligned}
\]
- [Q] I have a problem with the above statement. I think the correct form is that the change of the length of the curve approaches $0$.


- [Q] I think this is calculus of variations. How is it explained on Wikipedia / by some other source?


The form of equation {S} is reminiscent of the *action* in classical mechanics, which is written as
\[
\begin{aligned}
S_{\alpha \beta} &= \int_{t_1}^{t_2} L(x,p) \; dx
\end{aligned}
\]

where $L(x,p)$ is the Lagrangian and represents the total energy of the system. Here, the Lagrangian is:
\[
\begin{aligned}
L(x,p) &= \sqrt{\epsilon g_{\mu \nu} p^\mu p^\nu}
\end{aligned}
\]

If $\delta S_{\alpha \beta} \to 0$, then
\[
\begin{aligned}
0
\end{aligned}
\]

- [Q] Why are $x(t)$ and $p(t)$ both involved? $p(t)$ can be found from $x(t)$; why not express everything in terms of $x(t)$? Why treat $x(t)$ and its derivative wrt $t$ as independent variables? The derivative of $x(t)$ does not intuitively seem independent of $x(t)$. Explore this. Maybe it is. If it is, what is the use of using only $x(t)$ and its first derivative in the action?


Derivative of action with respect to t.

Integrate by parts.
Doing this produces the Euler-Langrange equation.

Goal now is to remove $\epsilon$ from the equation.
- [Q] Why? Simply simplicity (fewer variables in final formula)?

To do this, set $L = \sqrt{\epsilon w} \Rightarrow $

'right hand side can be written as quotient of two quantities'

- derives the geodesic equation

\[
\begin{aligned}
\frac{d^2 x^\sigma}{ds^2} + \Gamma_{\lambda \nu}^{\;\;\;\sigma} \frac{d x^\lambda}{ds} \frac{dx^\nu}{ds} &= 0
\end{aligned}
\]

As this is a 'tensor equation', it has the same form in all co-ordinate systems.
- [Q] What does it mean to be a 'tensor equation'?
- [A] If a change of co-ordinates is performed, the form of the equation is invariant/unchanged



----
## Useful distillations

- [Q] Why are both $x(t)$ and $p(t)$ present in a Lagrangian, perhaps implying they behave independently, and why is $q(t)$ not written as $\frac{d x(t)}{dt}$?
- [Q] Why are $x(t)$ and $\frac{dx(t)}{dt}$ present in the Lagrangian, but not $\frac{d^2 x(t)}{dt}$ or any higher order derivatives?
- [Q] Why is Euler-Lagrange equation, action minimization exactly equivalent to minimization of path integral? What does the Euler-Lagrange equation represent usually (can that thing be represented as a path in an arbitrary space?)?
- [Q] How is the geodesic equation derived? (e.g. Schuller lectures)


https://www.youtube.com/watch?v=4YPfFGRw_iI
https://www.youtube.com/watch?v=EceVJJGAFFI



.
