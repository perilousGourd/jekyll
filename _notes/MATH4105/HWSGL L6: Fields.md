
## Bundle

A bundle is a triple (3-tuple, ordered collection of three pieces of information) of two smooth manifolds $E$ and $M$ and a surjective smooth map $\pi$, called the projection map (of the bundle), $(E,\pi,M)$.

The projection map maps the first smooth manifold, called the 'total space' to the second smooth manifold, called the 'base space'.

This can be mathematically written
\[
\begin{aligned}
E \to^\pi M
\end{aligned}
\]
or
\[
\begin{aligned}
\pi: E\to M
\end{aligned}
\]
for total space $E$, base space $M$ and projection map $\pi(e)$ for some element $e \in E$.




## Smoothness
- [Q] What is a smooth map?
- [Q] What is a smooth manifold?

### Smooth maps
A map is smooth if it is differentiable everywhere.


### Smooth manifolds



.
