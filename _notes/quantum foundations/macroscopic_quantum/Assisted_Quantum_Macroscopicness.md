---
layout: post
title: Assisted Quantum Macroscopicness
---

# Yes?

## Summary of paper
- [ ] What did this paper aim to show/achieve?
- [ ] What did this paper show/achieve?

The macroscopicity problem is the question of whether quantum effects should be observable in arbitrarily large and complex systems (systems with arbitrarily many degree of freedom).

{:.q}
Has this been observed in practice, and, if not, why not? (What are the limiting factors?)

{:.a}
<div markdown=1>
The more particles a system has, the more space it occupies physically, and the less practical it is to keep the system closed. If the system is not closed, information lost to the environment would make quantum effects ...?
</div>

{:.q}
Why does losing information to the environment impact the likelihood of observing quantum effects in the system?
</div>



Is it that there are many degrees of freedom, or something to do with how the particles of the system interact?


{:.q}
How is an entangled state and a mixed state indistinguishable after measurement?

Measurements of fifty particles that are each in the entangled state $\|\psi\rangle = \frac{1}{2}\|0\rangle + \frac{1}{2}\|1\rangle$ and fifty measurements of particles from an ensemble in which half are in the state $\|\psi\rangle = \|0\rangle$ and half are in the state $\|\psi\rangle = \|1\rangle$ will return, on average, $25$

Usually, decoherence


{:.q}
What mathematically is decoherence?

{:.a}
<div markdown=1>
Mathematically, decoherence occurs when
</div>


{:.q}
What are the physical consequences of decoherence?

.
