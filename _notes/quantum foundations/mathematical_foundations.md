---
layout: post
title: Mathematical foundations of QM 1
---

## Degeneracy
- [ ] Is the fundamental concept redundancy?

### Degeneracy (of a vector space)

{:.unsure}
I suspect the term 'degeneracy' might apply to when multiple vectors in some set are not orthogonal / where there is some component of the space they exist in (that possesses orthogonal dimensions) that is shared between them.

### Degeneracy of a map
A map is


## Tensor product

A tensor product is a binary map on two vector spaces, a map that maps two elements, one from each vector space, to an element of a third vector space. A tensor product does not exist for mathematical objects that are not vector spaces.

It can also be phrased that a it is a bilinear composition of two vector spaces, where a process where two input objects become one is a 'composition' of the two input objects, and bilinear means the effects of transformations/manipulations to either of the input objects are linearly propagated through to the output object. For example


[toremove:] tensor product 'generalizes the outer product', so it is likely a vector space with four basis states results from the tensor product of two vector spaces with two basis states in each.

.
