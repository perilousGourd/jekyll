# Types of spaces

## What are spaces made of?

Spaces are sets of elements, with a sense in which the elements can be combined to produce other elements in the set. If a set and the sense of combining elements satisfies some criteria, the collection of {the set of elements and the combination rule} is called a space.

### Sets

### An addition structure that combines elements to give elements also in the set


### Operators / maps

## Spaces

### Vector space

### Inner product space

### Hilbert space

A Hilbert space is a vector space with an inner product structure (the ability via definition to perform the inner product operation with any two elements of the set / vectors in the space) that is also complete.

Does the number of dimensions of the Hilbert space impact whether it is complete? (Are only infinite-dimensional Hilbert spaces complete?)

#### Completeness
A vector space is complete if every Cauchy sequences of elements in it converges to an element also in it.

The most impactful implication of completeness is that calculus can be performed, as [check: limits converge, and converge to accessible points (points in the set)].

#### Isometric and isomorphic
An isometry is a mapping between two metric spaces (vector spaces with an operation that defines distance between all pairs of elements) that preserves the distances between all elements (points/vectors)
- [Q] Can an isometry apply to any object that is not a vector space?

An isomorphism is a mapping between two ? that preserves any relationships between elements, e.g. which map to which under the operation of the group (not necessarily distances)
Can a mapping between two sets that each have no structure be an isomorphism? I suspect so.


Terminology should be
Vector spaces with vectors
Point spaces with points
Meaning: Sets of elements that behave like vectors that have an addition operation (and thereby a scalar multiplication operation via repeated addition of an element with itself) and satisfy [the eight axioms of a vector space].

Terminology is
Vector spaces with points

(Meaning: and Intuitive terminology: and Terminology used: sections)




.
