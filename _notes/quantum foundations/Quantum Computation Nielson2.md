---
layout: post
title: Quantum Computation Nielson 2
---
## Section 2.4: The density operator


The density operator of a state is the outer product of the state with itself, giving an element of the output set for every possible combination of two basis states (each state with one other).
\begin{aligned}
\rho &:= \sum_i p_i \| \psi_i \rangle \langle \psi_i\|
\end{aligned}

{:.q}
What is the benefit of having a matrix with the vector multiplication of all possible dimensions (assuming diagonalized, efficiently represented state) / the outer product of the state with itself? What does each entry then correspond to physically?

{:.unsure}
The outer product of vector space with

It is quite natural that the matrix representing the outer product of a vector space with itself is diagonal, as

{:.q}
What does it mean to multiply two vectors geometrically? I'm aware this corresponds to the notion of 'area' (where area is how many elements exist after a unit-wise multiplication of the vectors by each other), but I think there is some geometrical significance of the position of the vector that would result (how does a vector result, rather than the number of units (a quantity, scalar), just described? This was in FFF).

The geometrical interpretation of area suffices in this case, as the product of two orthogonal vectors is $0$ via kronecker-delta, so

NOTE: Not unit-wise multiplied, as this would mean two orthogonal vectors have the greatest product.

Outer product of two vectors is the inner product of each basis vector in one vector with each in the other, for each possible combination of basis vectors. The inner

Inner product conceptually represents a kind of 'shared action', 'simultaneous impact' on elements of the vector space and ability to influence each other.

There is no sense, when vectors are from different vector spaces, of the coincidence of their basis vectors. There is no means to resolve whether the inner product of a basis vector in vector space $A$ and a basis vector in vector space $B$ is $0$, or $1$ or anywhere in between, no way to, in the analogue of dim-3 space, 'orient' the basis vectors relative to each other. In the analogue of dim-3 space, any two sets of basis vectors could be chosen, and the knowledge that they are independent of the other basis vectors in their set has no bearing on how they relate to / are dependent on basis vectors in the other set. Thus a tensor product is the 'unresolved'/'uncompiled'/'un-reduced' combination of every basis vector in one vector space with every basis vector in the other.



### Entanglement
Testing

\begin{aligned}
\|\psi\rangle &= \frac{1}{\sqrt{2}} \left(\|00\rangle + \|11\rangle \right) \\\\\\
\|\psi\rangle &= \frac{1}{\sqrt{2}} \left( \|0\rangle_A \otimes \|0\rangle_B + \|1\rangle_A \otimes \|1\rangle_B \right)
\end{aligned}

Thus $\| \psi \rangle$ cannot be formed by linearly combining vector spaces.


## Section 2.5: EPR and the Bell inequality



What was I tasked with considering?

on topic of
entanglement looks the same when measured afterwards as a mixed state (mixed due to lack of knowledge)

Bob was to do something, and I was to consider what it looked like from Alice's perspective, writing 'Alice's memory' in ket form.

Alice has two quantum coins


memory might return in process of:
- reproducing the process of preparing mixed states, sending entangled states and then measuring, etc
- has some relevance to the paper Tim was co-author on

approach:



.
