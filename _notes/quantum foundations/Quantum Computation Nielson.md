---
layout: post
title: Quantum Computation Nielson notes
---

NOTE: Take an attitude while writing these that they are for my genuine learning and future learners, note/learn genuinely, with assumption that can edit afterwards / censor for learners unlike myself
- [x] check formatting via stylesheets through these notes
- [x] formatting: different colour/class for outstanding vs resolved questions
- [ ] density matrices
- [ ] EPR and Bell's inequality
- [ ] Shahandehe paper
- [ ] BB84 QKD protocol
- [ ] any brief mathematical quantum tangents into mathematical quantum separate notes page (attempt to avoid this for now)
- [ ] incorporate images?
- [ ] upload to GitLab
- [ ] finalization of notes (e.g. removing links besides those relevant etc)

## How is it known / what are the justifications that there are no hidden variables / that QM is correct?

In *Section 1.2: Quantum bits* of *Quantum Computation and Information* is the statement that /
### Fundamental postulates of QM and why people are convinced of / confident in them

- quantum objects evolve while not being interacted with in a way that

{:.q}
'interacted with', or 'observed' in a consciousness-relevant sense?

{:.q}
Why can't the measurement made can be modelled probabilistically, and is there some way to model it deterministically?

## Current model of QM

### Evolution of states (how the states change with time)

- [Note] The word 'state' is overloaded, commonly used to refer to both the basis states (also called the eigenstates and the eigenvectors) of a system, which are the only states a quantum system has ever been measured to be in, and the pre-measurement superposition of all basis states. I use 'basis state' to denote the former and 'state' to denote the latter.

The state of a quantum system $| \psi \rangle$ evolves over time in a deterministic fashion. Thus, if the state was known at time $t_1$ to be $| \psi(t_1) \rangle$, it can be very consistently and accurately be predicted to be in the state
\[
\begin{aligned}
| \psi(t_2) \rangle &= U | \psi(t_1) \rangle
\end{aligned}
\]
at time $t_2$, where $U$ is a unitary operator.

A unitary operator is an operator that maps an element of a vector space to an element of the same vector space that has the same length, as defined by the metric of that space.

{:.q}
What is the difference between this and preserving the inner product?

{:.a}
The inner product of a vector with itself gives the length of the vector in the current basis. If a metric is incorporated, a value can be obtained that represents a basis-agnostic length (as the metric will change with change of bases to account for / 'cancel/balance out' the changes in the value of the length caused by the changing basis, to give a value that is invariant across change of basis).
\\
It thus means the same to 'preserve the inner product' as to 'perform a unitary operation' / it is thus the case that performing a unitary operation will preserve the inner product (given that the basis is unchanged, the inner product of the vector with itself before being unitarily mapped to another vector in the vector space will have the same value as the inner product of the image vector, the vector the first is mapped to).

itself before being unitarily mapped to another vector in the vector space will have the same value as the inner product of the image vector, the vector the first is mapped to).



- [Interesting] It is possible for an operator to be unitary and not describe a process that physically occurs [QCAI page 81].
  - In single qubit systems, however (single qubits not interacting with an outside world, such that the single-qubit system is 'closed'), the impact/manipulation of any unitary operator can be physically realized/represented.

{:.qr}
Why are unitary operators important in QM?

{:.a}
The length of a state vector is related to the probability []. Given that matter cannot be created or destroyed, a state cannot

#### Examples of unitary operators

##### The Pauli matrices
The Pauli matrices are unitary operators. There are multiple sets of them: one set of four for operations/transformations in a two dimensional vector space, and one set of ? for transformations in a three dimensional vector space.
- [Q] Are there any other sets of Pauli matrices?

They map an element of a dim-2 vector space to another element in the space. (They map a vector in a dim-2 vector space to another in the same space.)

- [Q] What are the two dimensions?
- [A] The two dimensions can represent the 'up' and 'down' basis states of a spin-$\frac{1}{2}$ particle, like an electron. The two dimensions can represent any two independent characteristics of an object (where those two characteristics are the only two possible).

Together, each set of Pauli matrices can perform each (linear?) transformation possible in a vector space of the corresponding number of dimensions (when allowed to act multiple times and in any order and combination with each other).

##### Hadamard gate
\\[
\begin{aligned}
H &= \frac{1}{2} \begin{pmatrix}
1 & 1\\
1 & -1
\end{pmatrix}
\end{aligned}
\\]




{:.qr}
What is the effect of the Hadamard gate on the physical state of the particle, how does the matrix represent this and how is it practically used?

{:.a}
<div markdown=1>
When the Hadamard gate operates on the basis state, it transforms it into / maps it to a balanced, equally weighted entangled state of the two basis states (note the Hadamard gate is only relevant to dim-2 vector spaces (which by Gram-Schmidt have two basis states)).

This can be represented mathematically via

\begin{aligned}
H |0\rangle &= \frac{1}{\sqrt{2}}\left( |0\rangle + |1\rangle \right)
\end{aligned}

The Hadamard gate transforms/maps the second basis state to an equally anti-weighted entangled state.

\begin{aligned}
H|1\rangle &= \frac{1}{\sqrt{2}} \left( |0\rangle - |1\rangle \right) \label{test}
\end{aligned}
</div>

equation \ref{test}


{:.qr}
What are the consequences/implications of the negation?

{:.a}
implications include chirality: ability to rotate (as asymmetry in directions available / one direction and a single 'rotated' direction, or direction to rotate in

{:.a}
the information exists that allows these states to be distinguished by measurement of one observable and not the other (will measure amount of component $\|0\rangle$ of each state, and opposite amounts of $\| 1\rangle$ state. What does it mean for the opposite of a component to be the negative amount of that component, rather than none of that component (the perhaps intuitive interpretation of 'opposite'?))

{:.qr}
What is an entangled state?

{:.a}
<div markdown=1>
I think an entangled state is just a state that a linear combination of basis states (and therefore that cannot be represented as a single basis state).

- There is also the 'that cannot  be decomposed into a product of basis states' part of the typical definition. I suspect this is useful for probability calculation/tooling? What are the physical and geometrical mathematical consequences of this? (Geometrical meaning of multiplication of basis states with each other? Outer product? Parallelogram, as FFF? This would be scalar. Should the product of basis states be multi-dimensional?)
</div>




[rearrange:] Eigenstates act like basis states for the vector space they are an eigenstate of (anything accessible in a vector space can be accessed by a linear combination of the eigenstates of that vector space).
[todo:] check this is true.
[question:] Does every vector space have eigenstates? I think I've read previously that some matrices don't have eigenstates. Have I misremembered/misinterpreted my incomplete memories?

###### Hermitianness

A Hermitian operator is one for which

Matrix equal to complex conjugate transpose. Implications for operator transformations are?

When represented visually as a matrix, this appears as the portion above the diagonal being equal to the complex conjugate of the portion below the diagonal, with each element of the matrix associated with the one that lies at its diagonal reflection.

The Hamiltonian operators used in QM (in the Schrodinger equation to find how states evolve with time) are Hermitian. As a consequence, they ?

[todo:] preserve state normalization/length and thus conserve matter-energy?
[question:] Why should Hamiltonians be Hermitian?


###### Self-adjoint vs Hermitian

The adjoint of an operator is the conjugate transpose (also called Hermitian transpose) of that operator, adapted (generalized) to make sense for potentially 'infinite'-dimensional vector spaces.

A self-adjoint operator is an operator that is adjoint to itself, that is, is its own conjugate transpose.

A Hermitian operator is a bounded self-adjoint operator.

For a set to be bounded in general means that every element of the set must be less than some finite value $M$. This means there must be a notion of size for the set's elements relative to each other for the notion of boundedness to exist. Of all the values that could possibly exist in the set,

##### Spectral decomposition of Hermitian (self-adjoint) operators

Hermitian matrices / operators can be represented as a linear combination of basis states, via Gram-Schmidt. The collection of their basis states is referred to as their spectrum.
[todo:] Is this what this is? I suspect not, as outer product should be involved?

This can be represented mathematically as
\begin{aligned}
H &= \sum_i E_i |\psi_i\rangle\langle\psi_i|
\end{aligned}

{:.qr}
<div markdown=1>
Why are the states that linearly compose (are weighted summed to give) the Hermitian operator called 'stationary states'?
</div>

{:.a}
<div markdown=1>
The states that linearly compose the Hermitian operators are called stationary states because the effect of evolution/mapping by a unitary operator is to keep them on the same ray in the Hilbert space; the composition of basis states that constitute them (a single basis state) is not changed by unitary evolution. The state may be scaled by a potentially complex number, but the constitution, proportionally, of basis vectors of it will be constant.
</div>



##### Quantum gates

- $\textrm{NOT}$ gate


There are four (sometimes three, with the identity conventionally omitted) Pauli matrices are unitary operators.



## Section 2.1: Linear Algebra

### Gram-Schmidt decomposition

### Pauli matrices



### Density matrices

{:.q}
What does each entry in a density matrix represent?

A density matrix is formed by the outer product of two state vectors.

A single state will produce a single density matrix.

If there could be multiple, different states in the ensemble of states (and which is about to be measured not known due to the experimenter's classical lack of knowledge), the density matrix describing the system will be constructed as the weighted average of / from multiple matrices] outer product of every state vector with itself will produce a matrix, and the average of these matrices (weighted by how classically likely the state that made them was to occur) will be the density matrix of what is then called the mixture. A mixture occurs when multiple states are present, and could possibly be measured.

{:.q}
What take the outer product of each state with itself? What does each entry of a matrix formed as an outer product of a state vector with itself represent?

[image: DMOP]










## Section 2.2: The postulates of QM


A pure state in QM is a unit norm vector in a Hilbert (vector) space. This state may change as the universe interacts with it. The way the effect of the interaction of the universe on the state is modelled is via maps of the state vector to other elements (vectors) of the same Hilbert space. These maps are self-adjoint (more general form of Hermitian, which applies only to finite-dimensional complex inner product spaces (as in finite-dimensional Hilbert spaces? would require completeness also)) operators ().

The state is (proposed to) change as a result of being measured. measuring

{:.qr}
It seems strange that the way it is changed by being measured is exactly the same as what

{:.a}
<div markdown=1>
By QM, measuring the state changes the state. The inner product of the evolved/new state and the original is the value that is obtained for the measurement.
The expectation value of an observable, e.g. spin, or position, when the state of the system is known, is $\langle x, Ax \rangle$. An implication of this is that
</div>

{:.qr}
Does the direction the state vector points represent the probability of the state having some value or represent the possible values the state could have?

{:.a}
<div markdown=1>
The bases of the space the state vector points in represent the possible values (one basis vector for each possible value / measurement outcome: e.g. two basis vectors for a spin half particle, one for spin up and one for spin down, and an 'infinite' number of basis vectors for the 'continuum' of positions in a 'continuous' space, one for each position. This latter example is the sense in which infinite-dimensional Hilbert spaces can exist.) The direction the state vector points corresponds to the probability of the system's state being measured to be in each basis state. Only the basis states are observable via measurement.
</div>


### Finite-dimensional spectral theorem
Self adjoint operators on complex inner product spaces can be written as real-valued diagonal matrices.



### EPR pairs / Bell basis states

\begin{aligned}
\|\psi\rangle &= \frac{1}{\sqrt{2}} \left(\|00\rangle + \|11\rangle \right) \\\\\\
\|\psi\rangle &= \frac{1}{\sqrt{2}} \left( \|0\rangle_A \otimes \|0\rangle_B + \|1\rangle_A \otimes \|1\rangle_B \right) \newline \\\\\\
\|\psi\rangle &= \frac{1}{\sqrt{2}} \left(\|00\rangle - \|11\rangle \right) \\\\\\
\|\psi\rangle &= \frac{1}{\sqrt{2}} \left(\|10\rangle + \|01\rangle \right) \\\\\\
\|\psi\rangle &= \frac{1}{\sqrt{2}} \left(\|01\rangle - \|10\rangle \right)
\end{aligned}





## Section 2.4: The density operator

### Entanglement
Testing

\begin{aligned}
\|\psi\rangle &= \frac{1}{\sqrt{2}} \left(\|00\rangle + \|11\rangle \right) \\\\\\
\|\psi\rangle &= \frac{1}{\sqrt{2}} \left( \|0\rangle_A \otimes \|0\rangle_B + \|1\rangle_A \otimes \|1\rangle_B \right)
\end{aligned}

Thus $\| \psi \rangle$ cannot be formed by linearly combining vector spaces.


## Section 2.5: EPR and the Bell inequality



What was I tasked with considering?

on topic of
entanglement looks the same when measured afterwards as a mixed state (mixed due to lack of knowledge)

Bob was to do something, and I was to consider what it looked like from Alice's perspective, writing 'Alice's memory' in ket form.

Alice has two quantum coins


memory might return in process of:
- reproducing the process of preparing mixed states, sending entangled states and then measuring, etc
- has some relevance to the paper Tim was co-author on

approach:



.
