# Geometrically understanding/exploring quantum spin, FFF p170
## 2.4
- asserts that Copenhagen interpretation only makes statements about knowledge of a system, not behaviour of the system. I thought the measurement problem was inherent in Copenhagen interpretation?

## 2.5
- key mathematical property of quantum is that sum of the evolutions of quantum particles is the same as the evolution of the sum of the particles
  - that it doesn't matter whether particles are 'aware' of each other / 'combined' when / prior to evolving in time, and therefore that the action/operation of 'combining' quantum systems in this way does not impact their identity (at least not their behaviour, and possibly not their identity. Perhaps their identity is affected, but not detected, and this is how this seemingly stochastic behaviour occurs (this is hidden variable))


- I think the 'combination' without impacting identity being mentioned, called 'superposing', is of basis states and due to the linearity of orthogonal spatial dimensions
- phase relationships, like those I've been independently exploring in density matrix noteset, impact the *interference* between the states


Two basis states can be combined (superposed), and the evolution of their combination (represented as $\phi$) can be considered.
\[
\begin{aligned}
\phi &= w \hat{\alpha} + z \hat{\beta}
\end{aligned}
\]
The combined state will evolve in time in (the same or different? Is implying both, I think) in a way that affects likelihoods of either basis state being observed

- [Q] probabilities of basis states oscillate in time? (What happens (mathematically) if only one state is possible, and what is the transition mathematically between this case and the case of multiple possible basis states?)
  - 'individual states' (presumably basis states?) can reinforce or cancel each other.
    - [Q] How the heck can this happen? This breaks linearity / lack of impact immediately?
    - Perhaps states that are combinations of basis states are being referred to here, and the basis states reinforce the same basis states across the superpositions, with each basis state therefore only interacting with itself?



- [Q] How does the Schrodinger equation interact with / how is time-evolution of it affected by the Heisenberg uncertainty principle / incompatible observables?

Suggests in this section that precision is position would cause the state to 'spread immediately outwards' (in space, I assume, if position is known, and in whatever other observable that is known with precision at any time) due to contributions of 'high momentum states'.

### Wessel plane
- complex plane




.
