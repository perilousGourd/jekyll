---
layout: post
title: Logic of prepositions and predicates
---


This is a course, in the form of a series of online lectures, recorded by the brilliant Frederic Schuller.

The aim of this course is to introduce a rigorous, technically consistent and thus valid mathematical language for
- classical mechanics
- electromagnetism
- quantum mechanics
- statistical physics

{:.unsure}
Note that the mathematical language for each of these is, while consistent and following from ultimately the same axioms (of set theory), typically grouped into different fields, as uses different collections of (compatible) methods with little overlap in application to the other fields. (Like bunches of leaves of a tree not sharing the same branch while sharing the same trunk.)


Mathematical fields most relevant to physics:
- Analysis
- Linear algebra is used in quantum mechanics (note: is the modern implementation of quantum, may not have been the original?)
- Geometry is used in statistical physics via, for example 'convex cones of states'

This course is mostly about differential geometry.

The foundations of differential geometry is set theory.

The spaces in EM, QM, the physical space that Hilbert spaces represent in quantum, are sets.

Sets are collections of elements.

As sets are the most fundamental objects, are described axiomatically with goal to capture the notion of a set most accurately and usefully.
Logic is the language with which mathematics is most fundamentally described. Logic is used to describe the axioms of set theory.

{:.idea}
It strikes me that the notion of continuity (capturing that the paths of particles throughout space, for example, don't 'jump') can be captured without infinitesimals, and simply via an order of discrete positions, where information can only be transferred between nearest neighbours of a discrete three dimensional grid (for the apparent three spatial dimensions of this universe).


Topology:

## Topological manifolds:
- There is no such thing as a 'position vector'
  - a position vector is actually a collection of coordinates (that are not actually a vector)

## Bundles:
- There is no such thing as a 'wavefunction' is quantum mechanics.
  - a wavefunction is not a function, but is a section of a complex line bundle over physical space


The words 'position vector' and 'wave function' can loosely used in physics, but are technically invalid and confusing if scrutinized.

Lorentzian metric <span style="display:inline-block; width:1ex; height:1ex; background-color:red"></span>


<img src="lopap/built_on.svg" width="500px">


# Axiomatic set theory

## Propositional logic

A proposition $p$ is a 'variable' that can take (only) the values $\text{TRUE}$ or $\text{FALSE}$. Equivalently, $p$ is binary.

\[
\begin{aligned}
testing &= \frac{1}{2} |0\rangle
\end{aligned}
\]

{:.q}
Testing a question class!

{:.a}
And this would **be the** answer!

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

####### Heading 7
.
